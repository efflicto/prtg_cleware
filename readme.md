## PRTG Cleware
- A small Python application to read the [Cleware humidity and temperature](http://www.cleware-shop.de/epages/63698188.sf/de_DE/?ObjectPath=/Shops/63698188/Products/06/SubProducts/06-1) sensor.
- Only tested on Linux and Python 3.6.
- You need to have [Clewarecontrol](https://github.com/flok99/clewarecontrol) installed.
- You need to have the modules installed listed in the requirements.txt
- Add a new "HTTTP Advanced Sensor" in PRTG
- Copy the config.example.py and replace the variables there with your values from the sensor/server
- Run it as with cronjob
- Used some snippets from [here](https://kb.paessler.com/en/topic/74397-where-do-i-find-the-script-to-monitor-server-room-temperature-on-a-budget#reply-251199)

![image](http://i.imgur.com/I8KLcWV.png)