# -*- coding: utf-8 -*-
# !/usr/bin/env python3
import json

import cleware.cleware
import cleware.util
import config
import logging
import os
import requests
import sys
from logging.handlers import RotatingFileHandler

# Logging
logger = logging.getLogger("logger")
logger.setLevel(logging.DEBUG)
handler = RotatingFileHandler(
    os.path.join(os.path.dirname(os.path.realpath(__file__)), 'log.log'),
    maxBytes=100000,
    backupCount=10)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s [prtg_cleware] [%(levelname)-5.5s]  %(message)s")
handler.setFormatter(formatter)
logger.addHandler(handler)
streamHandler = logging.StreamHandler()
streamHandler.setFormatter(formatter)
logger.addHandler(streamHandler)

debug_mode = False


def main():

    try:
        logger.info("PRTG Cleware startet")
        # Find all Cleware devices
        logger.info("Gathering Cleware devices")
        devices = cleware.util.get_cleware_devices()
        found_devices = []
        for device in devices:
            found_devices.append(f"{device} - {devices[device][0]}")
        logger.info(f"Found devices: {found_devices}")

        # Read all devices and gather all sensor values
        sensor_values = {}
        for device in devices:
            logger.info(f"Reading device: {devices[device][0]} - {device}")
            cl = cleware.cleware.Cleware(id_product=devices[device][1].idProduct)
            values = cl.get_values()
            for value in values:
                sensor_values[f"{devices[device][0]} - {device} - {value}"] = values[value]
            cl = None
        logger.info(f"Reading done. Got the following values: {sensor_values}")

        # Send data to PRTG
        if not debug_mode:
            # prepare json payload for PRTG
            logger.info("Preparing PRTG payload")
            prtg_payload = {
                "prtg": {
                    "result": [
                    ]
                }
            }
            for value in sensor_values:
                if "Temperature" in value:
                    prtg_payload["prtg"]["result"].append(
                        {
                            "Channel": value,
                            "Float": 1,
                            "Unit": "Temperature",
                            "DecimalMode": "All",
                            "Value": sensor_values[value]
                        }
                    )
                if "Humidity" in value:
                    prtg_payload["prtg"]["result"].append(
                        {
                            "Channel": value,
                            "Float": 1,
                            "Unit": "Percent",
                            "Value": sensor_values[value]
                        }
                    )
            # clean data convert to string
            prtg_payload_json = json.dumps(prtg_payload)
            # Prepare request
            prtg_request = f"http://{config.PRTG_HOST}:{config.PRTG_PORT}/{config.PRTG_TOKEN}?content={prtg_payload_json}"
            # Send payload to PRTG via get request
            logger.info("Sending PRTG payload")
            request = requests.get(prtg_request)
            if request.status_code == 200:
                logger.info(f"Request sent {request.status_code}. Done.")
                sys.exit(0)
            else:
                logger.error(f"Error sending PRTG request: {request.status_code}")
                sys.exit(1)

    except Exception as e:
        logger.exception(e)
        sys.exit(1)


if __name__ == '__main__':
    main()
    sys.exit(0)
