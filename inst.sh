#!/bin/sh

# Test installation script. DONT USE IT!

sudo apt-get update && sudo apt-get -y upgrade
sudo apt-get install -y build-essential
sudo apt-get install -y git
sudo apt-get install -y libhidapi-dev
sudo apt-get install -y vim
sudo apt-get install -y python3-dev libffi-dev libssl-dev
sudo apt-get install -y zlib1g-dev
cd /tmp
wget https://www.python.org/ftp/python/3.6.2/Python-3.6.2.tgz
tar xzf Python-3.6.2.tgz
cd Python-3.6.2
sudo  ./configure --with-zlib-dir=/usr/local/lib
sudo make
sudo make altinstall
mkdir -p /home/cleware/
cd /home/cleware/
git clone https://gitlab.com/efflicto/prtg_cleware.git
cd prtg_cleware
sudo pip3.6 install -r requirements.txt
cp config.example.py config.py
