import time
import usb.util
from setup_logger import logger


class Cleware:
    '''
    VENDOR: Cleware
    id_product: ID of the device: 0x0010 = USB-Temp 0x0020 = USB-Humidity
    '''
    def __init__(self, id_product):
        self.VENDOR = 0x0d50
        self.id_product = id_product
        try:
            self.device = usb.core.find(idVendor=0x0d50, idProduct=id_product)
        except Exception as e:
            logger.exception(e)
        self.device_ready = False

    # Prepare the device for use
    def prepare_device(self):
        try:
            if self.device.is_kernel_driver_active(0):
                self.device.detach_kernel_driver(0)
            self.device.set_configuration()
            self.device_ready = True
        except Exception as e:
            logger.exception(e)

    # Read the device data from the endpoint
    def read_device(self):
        if not self.device_ready:
            self.prepare_device()
        retries = 0
        data = None
        # Read multiple times, if no data from device
        while not data:
            if retries > 50:
                logger.error("Maximum retries reached. Abort reading.")
                break
            try:
                data = self.device.read(0x81, 1024, 100)
            except usb.core.USBTimeoutError:
                #logger.debug("Timeout reading device")
                retries = retries + 1
            except Exception as e:
                logger.exception(e)
                retries = retries + 1
        return data

    # Get the temperature of the device for USB-Temp or Humidity
    def get_temperature(self):
        # Cleware USB-Temp
        data = self.read_device()
        if not data:
            return None
        if self.id_product == 0x0010:
            # Convert data to actual values (see: https://github.com/flok99/clewarecontrol/blob/master/USBaccess.cpp)
            temp_raw = (data[2] << 5) + (data[3] >> 3)
            temperature = temp_raw * 0.0625
            return temperature
        # Cleware Humidity-Sensor
        elif self.id_product == 0x0020:
            # Convert data to actual values (see: https://github.com/flok99/clewarecontrol/blob/master/USBaccess.cpp)
            temp_raw = (data[4] << 8) + data[5]
            temperature = -40. + 0.04 * temp_raw
            return temperature

    # Get the humidity
    def get_humidity(self):
        data = self.read_device()
        # Convert data to actual values (see: https://github.com/flok99/clewarecontrol/blob/master/USBaccess.cpp)
        humi_raw = (data[2] << 8) + data[3]
        humidity = -4. + 0.648 * humi_raw - 7.2 * humi_raw * humi_raw / 10000
        return humidity

    # Get all values of device
    def get_values(self):
        values = {"Temperature": self.get_temperature()}
        if self.id_product == 0x0020:
            values["Humidity"] = self.get_humidity()
        return values
