import usb.util

CLEWARE_DEVICES = {
    0x0010: 'USB-Temp',
    0x0020: 'USB-Humidity'
}


def get_cleware_devices():
    cleware_devices = {}
    # Find all USB devices
    devices = usb.core.find(find_all=True)
    # Filter out Cleware devices
    iterator = 0
    for device in devices:
        if device.idVendor == 0x0d50:
            cleware_devices[iterator] = [CLEWARE_DEVICES[device.idProduct], device]
        iterator = iterator + 1
    # Return a dict of Cleware devices with the type of the device
    return cleware_devices
