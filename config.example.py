# -*- coding: utf-8 -*-
# !/usr/bin/env python3

PRTG_HOST = 'PRTG_HOST'
PRTG_PORT = 'PRTG_SENSOR_PORT'
PRTG_TOKEN = 'PRTG_SENSOR_TOKEN'
