import logging
import os
from logging.handlers import RotatingFileHandler

logger = logging.getLogger("prtg_cleware")
logger.setLevel(logging.DEBUG)
handler = RotatingFileHandler(
    os.path.join(os.path.dirname(os.path.realpath(__file__)), 'log.log'),
    maxBytes=100000,
    backupCount=10)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s [prtg_cleware] [%(levelname)-5.5s]  %(message)s")
handler.setFormatter(formatter)
logger.addHandler(handler)
streamHandler = logging.StreamHandler()
streamHandler.setFormatter(formatter)
logger.addHandler(streamHandler)